all: bin/estatico bin/dinamico
	 
bin/estatico: obj/segundos.o obj/dias.o obj/main.o
	mkdir -p bin/
	mkdir -p lib/
	ar rcs lib/libconv.a obj/segundos.o obj/dias.o
	gcc -static obj/main.o -L lib/ -lconv -o bin/estatico
	
bin/dinamico: obj/segundos.o obj/dias.o obj/main.o
	gcc -shared -fPIC src/segundos.c src/dias.c -o lib/libconv.so
	gcc obj/main.o -L lib -lconv -o bin/dinamico
obj/segundos.o: src/segundos.c
	mkdir -p obj/
	gcc -Wall -c src/segundos.c -o obj/segundos.o
obj/dias.o: src/dias.c
	gcc -Wall -c src/dias.c -o obj/dias.o 
obj/main.o: src/main.c
	gcc -Wall -c -I include src/main.c -o obj/main.o
	
clean: 
	rm obj/* bin/* lib/*
